package br.com.alura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApicontroleorcamentofamiliarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApicontroleorcamentofamiliarApplication.class, args);
	}

}
